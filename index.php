<?php
    $errors = [];
    if($_SERVER["REQUEST_METHOD"] == 'POST'){

        $allowedExtension = ['image/jpeg', 'image/png'];

        if(in_array($_FILES["media"]["type"], $allowedExtension)){
            $fileName = uniqid().'.'.explode('/',$_FILES["media"]["type"])[1];
            move_uploaded_file($_FILES['media']['tmp_name'], 'uploads/' . $fileName);
        } else{
            $errors[] = 'Le type de fichier n\'est pas bon';
        }
    }
?>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Ajout d'un fichier</h1>

    <form method="post" action="index.php" enctype="multipart/form-data">
        <label for="uploadFile">Upload de fichier !</label>
        <input type="file" name="media">
        <input type="submit">
    </form>

    <h2>Les fichiers de notre médiathèque</h2>


    <div class="row">
    <?php
        $dir = scandir("uploads");

        foreach ($dir as $file){
            if($file != '.' && $file != '..'){
                echo('<div class="card" style="width: 18rem;">
 
  <div class="card-body">
    <img class="card-img-top" src="uploads/'.$file.'" alt="Card image cap">
</div> </div>');
            }
        }
    ?>
    </div>

    <?php
    // Ici on affiche le tableau avec toutes nos erreurs !
    foreach ($errors as $error){
        echo('<div class="error alert-danger">'.$error.'</div>');
    }
    ?>


    <script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</div>
</body>
</html>